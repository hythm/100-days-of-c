#include

/*This is a comment. */
int main(int argc, char *argv[])
{
int distance = 100;

//this is also a comment
printf("You are %d miles away. \n", distance);
return 0;
}

/* (base) hythm@alrys:~/100-days-of-c/ex1$ make ex11
cc     ex11.c   -o ex11
ex11.c:1:10: error: #include expects "FILENAME" or <FILENAME>
 #include
          ^
ex11.c: In function ‘main’:
ex11.c:9:1: warning: implicit declaration of function ‘printf’ [-Wimplicit-function-declaration]
 printf("You are %d miles away. \n", distance);
 ^~~~~~
ex11.c:9:1: warning: incompatible implicit declaration of built-in function ‘printf’
ex11.c:9:1: note: include ‘<stdio.h>’ or provide a declaration of ‘printf’
<builtin>: recipe for target 'ex11' failed
make: *** [ex11] Error 1
*/
