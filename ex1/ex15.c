#include <stdio.h>

/*This is a comment. */
int main(int argc, char *argv[])

int distance = 100;

//this is also a comment
printf("You are %d miles away. \n", distance);
return 0;
}
/*(base) hythm@alrys:~/100-days-of-c/ex1$ make ex15
cc     ex15.c   -o ex15
ex15.c: In function ‘main’:
ex15.c:6:1: error: parameter ‘distance’ is initialized
 int distance = 100;
 ^~~
ex15.c:9:1: error: expected declaration specifiers before ‘printf’
 printf("You are %d miles away. \n", distance);
 ^~~~~~
ex15.c:10:1: error: expected declaration specifiers before ‘return’
 return 0;
 ^~~~~~
ex15.c:11:1: error: expected declaration specifiers before ‘}’ token
 }
 ^
ex15.c:4:5: error: old-style parameter declarations in prototyped function definition
 int main(int argc, char *argv[])
     ^~~~
ex15.c:11:1: error: expected ‘{’ at end of input
 }
 ^
<builtin>: recipe for target 'ex15' failed
make: *** [ex15] Error 1
(base) hythm@alrys:~/100-days-of-c/ex1$
*/
