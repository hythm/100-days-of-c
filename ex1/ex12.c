#include <stdio.h>

/*This is a comment. */
 main(int argc, char *argv[])
{
int distance = 100;

//this is also a comment
printf("You are %d miles away. \n", distance);
return 0;
}
/* (base) hythm@alrys:~/100-days-of-c/ex1$ make ex12
cc     ex12.c   -o ex12
ex12.c:4:2: warning: return type defaults to ‘int’ [-Wimplicit-int]
  main(int argc, char *argv[])
  ^~~~
*/
