# 100 Days of C

- My contribution to the Arabic initiative to create an Arabic version of [#100daysofcode](https://twitter.com/search?q=%23100daysofcode&src=typed_query) started by [@YasserTheDev](https://twitter.com/YasserTheDev) on twitter under the hashtag [#100يوم_من_البرمجة](https://twitter.com/hashtag/%D9%A1%D9%A0%D9%A0%D9%8A%D9%88%D9%85_%D9%85%D9%86_%D8%A7%D9%84%D8%A8%D8%B1%D9%85%D8%AC%D9%87?src=hashtag_click)

- I've been contemplating learning the C language for a while now; since it is the most appropriate language to complement my Electronics domain field, and this initiative seemed an excellent opportunity to start.

- I am using the [Learn C the Hard Way by Zed Shaw](https://www.amazon.com/Learn-Hard-Way-Practical-Computational/dp/0321884922) book and accompanied videos as my starting point.

- In this repo, I will upload the results of my exercises, and anything extra about C I find along the way.
