/* breaking it the 3rd */

#include <stdio.h>

int main()
{
int age ;
int height = 72;

printf("I am %d years old.\n", age);
printf("I am %d inches tall.\n", height);

return 0;
}

/* indentation has no effect here */
/*
(base) hythm@alrys:~/100-days-of-c/ex3$ ./ex33
I am 0 years old.
I am 72 inches tall.
(base) hythm@alrys:~/100-days-of-c/ex3$
*/
