/* Breaking the code experiment 1 */
/* removing age from first ptintf ./
/. then try to compile and see what warning i might get */

#include <stdio.h>

int main()
{
  int age = 10;
  int height = 72;

  printf("I am %d years old.\n",);
  printf("I am %d inches tall.\n", height);

  return 0;
}

/* results */
/*
(base) hythm@alrys:~/100-days-of-c/ex3$ make ex31
cc     ex31.c   -o ex31
ex31.c: In function ‘main’:
ex31.c:12:33: error: expected expression before ‘)’ token
   printf("I am %d years old.\n",);
                                 ^
<builtin>: recipe for target 'ex31' failed
make: *** [ex31] Error 1
(base) hythm@alrys:~/100-days-of-c/ex3$
*/
