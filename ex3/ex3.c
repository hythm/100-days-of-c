#include <stdio.h>

int main()
{
  int age = 10;
  int height = 72;

  printf("I am %d years old.\n", age);
  printf("I am %d inches tall.\n", height);

  return 0;
}

/* results */
/*
(base) hythm@alrys:~/100-days-of-c/ex3$ ./ex3
I am 10 years old.
I am 72 inches tall.
*/
